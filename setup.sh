#!/bin/bash

echo "Data Streaming Pipeline setup will begin..."

ID=data-streaming-iot-core
REGION=europe-west1

#Enable Cloud PubSub & Cloud IoT Core APIs
gcloud services enable compute.googleapis.com pubsub.googleapis.com cloudiot.googleapis.com 
echo "Cloud PubSub & Cloud IoT Core APIs are enabled..."

#Create a PubSub topic
gcloud pubsub topics create ${ID}-topic
echo "${ID}-topic pubsub topic created..."

#Create a Subscription for this PubSub topic created above for testing purpose
gcloud pubsub subscriptions create ${ID}-sub --topic=${ID}-topic
echo "${ID}-sub pubsub subscription created for testing purpose..."

#Create the Cloud IoT Core Device Registry
gcloud iot registries create ${ID}-registry --event-notification-config=topic=${ID}-topic --region=$REGION --no-enable-http-config
echo "${ID}-registry Cloud IoT Core Device Registry created..."

##Generate a device key pair prefer RS256
#openssl req -x509 -newkey rsa:2048 -keyout rsa_private.pem -nodes -out rsa_cert.pem -subj "/CN=unused"
#openssl pkcs8 -topk8 -inform PEM -outform DER -in rsa_private.pem -nocrypt > rsa_private_pkcs8
#echo "RS256 key paid generated..."

#Add a device to the registry using the keys you generated
gcloud iot devices create device-1 --registry=${ID}-registry --region=$REGION --public-key=path=./rsa_cert.pem,type=RS256
echo "device-1 is added to ${ID}-registry registry"

echo "Data Streaming Pipeline setup is completed..."
